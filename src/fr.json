{
    "LanguageName": "Français",

    "LoadingText": "Chargement...",
    "OkButton": "OK",
    "CancelButton": "Annuler",
    "YesButton": "Oui",
    "NoButton": "Non",
    "ContinueButton": "Continuer",
    "BackButton": "Retour",
    "ExitButton": "Quitter",
    "DefaultValueText": "{} (défaut)",
    "ErrorPopup": {
        "TitleText": "Erreur !"
    },
    
    "TitleScreen": {
        "DemoText": "DÉMO",
        "SingleplayerButton": "Solo",
        "MultiplayerButton": "Multijoueur",
        "TutorialButton": "Tutoriel",
        "SettingsButton": "Paramètres",
        "CreditsButton": "Crédits",
        "QuitButton": "Quitter le jeu"
    },
    
    "Singleplayer": {
        "YourWorldsText": "Vos mondes:",
        "NoWorldsText": "Il n'y a aucun monde à charger...",
        "InvalidWorldButton": "{} (invalide)",
        "OldWorldButton": "{} (ancien)",
        "NewWorldButton": "Nouveau monde",
        "WorldConversionPopup": {
            "TitleText": "Convertir au nouveau format de monde en vigueur ?",
            "OldWorldVersionText": "Le monde \"{}\" ne peut pas être chargé car il utilise un ancienne version du format de monde ({}), qui est incompatible avec la version actuelle ({}).",
            "OldWorldVersionlessText": "Le monde \"{}\" ne peut pas être chargé car il utilise un ancienne version du format de monde, qui est incompatible avec la version actuelle.",
            "ConversionPromptText": "Voulez-vous convertir le monde vers la version en vigueur ? Vous avez le choix de sauvegarder avant de procéder."
        },
        "InvalidWorldPopup": {
            "TitleText": "Monde invalide !",
            "InvalidVersionText": "Le monde \"{}\" ne peut pas être chargé, il provient peut être d'un version plus récente de 4D Miner ou a un numéro de version invalide."
        }
    },
    
    "CreateWorld": {
        "WorldNameText": "Nom du monde:",
        "WorldNameInput": "Nouveau monde",
        "WorldSeedText": "Seed/Graine du monde:",
        "FlatWorldCheckbox": "Monde plat",
        "GenerateCavesCheckbox": "Générer des grottes (plus lent)",
        "CaveSizeSlider": "Taille des grottes: {}%",
        "CaveFrequencySlider": "Fréquence des grottes: {}%",
        "CaveDetailLevelSlider": "Niveau de détail des grottes: {} (affecte gravement les performances)",
        "BiomeSizeSlider": "Taille des biomes: {}%",
        "TerrainAmplificationSlider": "Amplification du terrain: {}%",
        "CreateNewWorldButton": "Créer un nouveau monde !",
        "NoNamePopup": {
            "NoNameText": "Vous ne pouvez pas créer de monde sans le nommer !"
        }
    },

    "WorldConverter": {
        "CloseWarningText": "Ne PAS fermer le jeu !",
        "BackupWorldTitle": "{} (copie de sauvegarde)",
        "BackupStatusText": "Sauvegarde d'une copie du monde \"{}\"...",
        "ConversionStatusText": "Conversion du monde \"{}\" de la version {} à la version {}...", 
        "ConversionFinishedText": "Terminé !",
        "BackupPopup": {
            "TitleText": "Sauvegarder les fichiers du monde \"{}\"?",
            "PromptText": "Les données de sauvegarde du monde \"{}\" risquent d'être corrompu par le processus de conversion de monde. Souhaitez-vous d'abord faire une savegarde ?"
        },
        "ErrorPopup": {
            "GenericConversionErrorText": "Une erreur est survenue lors de la mise à jour du monde vers la version {}.",
            "InvalidObjectText": "Impossible de copier l'objet invalide \"{}\" (répertoire ou fichier régulier attendu).",
            "CopyErrorText": "Impossible de copier l'élément \"{}\": \"{}\"",
            "MissingChunksDirText": "Répertoire \"chunks\" manquant.",
            "UnableToOpenFileText": "Impossible d'ouvrir le fichier \"{}\"."
        },
        "ConversionStatus": {
            "CleaningUpText": "Nettoyage..."
        }
    },
    
    "WorldGen": {
        "ProgressText": "{}/{} chunks chargés"
    },

    "Settings": {   
        "ControlsButton": "Contrôles",
        "FullscreenButton": {
            "EnterText": "Mettre en plein écran",
            "ExitText": "Sortir du plein écran"
        },
        "RenderDistanceSlider": "Distance de rendu: {}",
        "ScrollSensitivitySlider": "Sensibilité de la molette:",
        "LookSensitivitySlider": "Sensibilité de la souris:",
        "InvertLookXCheckbox": "Inverser l'axe X de la souris",
        "InvertLookYCheckbox": "Inverser l'axe Y de la souris",
        "FovSlider": {
            "LabelText": "Champ de vision: {}",
            "ValueText": "{} degrés"
        },
        "DifficultySlider": {
            "LabelText": "Difficulté: {}",
            "EasyText": "Facile (aucun monstre)",
            "NormalText": "Normale",
            "HardText": "Difficile"
        },
        "SmoothLightingCheckbox": "Éclairage doux",
        "ShadowsCheckbox": "Ombres",
        "ColoredLightsCheckbox": "Sources de lumière colorée",
        "Forg": {
            "LabelText": "Forg: {}",
            "Names": {
                "Frank": "Frank",
                "Fernanda": "Fernanda"
            }
        },
        "Audio": {
            "LabelText": "Audio:",
            "GlobalVolumeSlider": "Volume global:",
            "MusicVolumeSlider": "Volume de la musique:",
            "CreatureVolumeSlider": "Volume des créatures:",
            "AmbienceVolumeSlider": "Volume des sons d'ambiance:"
        },
        "Multiplayer": {
            "LabelText": "Multijoueur:",
            "ChatCheckbox": "Afficher le Tchat",
            "NametagsCheckbox": "Afficher l'étiquette du nom des joueurs",
            "SkinsCheckbox": "Afficher les skins personnalisés"
        }
    },

    "Tutorial": {
        "Slideshow": {
            "Slide1": {
                "Text1": "Ce tutoriel est conçu pour vous aider à comprendre le fonctionnement d'un espace à quatre dimensions.\n\n\n\nEn comparant la différence entre un espace 2D et 3D, vous pouvez vous faire une idée du lien entre un espace 3D et 4D."
            },
            "Slide2": {
                "Text1": "Voici {} la forg (oui, une FORG)",
                "Text2": "Les Forgs sont des entités bidimensionnelles, ce qui signifie qu'elles peuvent exister seulement dans un plan en 2D, mais cela ne signifie pas qu'elles ne peuvent explorer la troisième dimension!\n\n\n\nLes Forgs peuvent se mouvoir dans quelconque coupe transversale 2D du monde 3D où elles existent."
            },
            "Slide3": {
                "Text1": "De plus, les Forgs ont la capacité unique de pouvoir faire pivoter leur plan physique dans la troisième dimension, ce qui leur permet de voir la section transversale qu'elles désirent !",
                "Text2": "Dans la prochaine partie, vous allez être capable de contrôler {} et expérimenter cette fonctionnalité par vous-même !"
            },
            "Slide4": {
                "Text1": "La même logique s'applique à la troisième et à la quatrième dimension.\n\n\nVous pouvez utiliser la molette de la souris pour faire pivoter votre vue de la quatrième dimension et découvrir de toutes nouvelles spécificités du terrain impossible à voir auparavant !",
                "Text2": "Vous êtes maintenant prêt à jouer à 4D Miner! Si vous souhaitez de l'aide pour naviguer dans la quatrième dimension, essayer la",
                "Text3": "Boussole!"
            }
        },
        "ForgGame": {
            "2DViewText": "ZQSD ou les flèches directionnelles pour déplacer {} la forg.\n\nUtiliser la molette de la souris pour faire pivoter la coupe transversale !",
            "3DViewText": "Cliquer + Glisser la souris pour ajuster la vue en 3D"
        }
    },  

    "Game": {
        "Chat":
        {
            "LabelText": "Tchat:"
        },
        "CraftingMenu": {
            "LabelText": "Fabrication:"
        },
        "Compass": {
            "PositionText": "Position: {0}, {1}, {2}, {3}",
            "FacingDirectionText": "Direction de la vue: {0}, {1}, {2}, {3}",
            "XText": "X: {}",
            "YText": "Y: {}",
            "ZText": "Z: {}",
            "WText": "W: {}"
        },
        "Chest": {
            "LabelText": "Coffre:"
        }   
    },

    "Pause": {
        "PausedText": "Pause...",
        "BackToGameButton": "Retour au jeu...",
        "QuitToTitleButton": "Revenir à l'écran titre"
    },

    "Items": {
        "Air": "Air",
        "Grass": "Herbe",
        "Dirt": "Terre",
        "Stone": "Roche",
        "Wood": "Bois",
        "Leaf": "Feuille",
        "SemiMoltenRock": "Roche semi-fondue",
        "IronOre": "Minerai de Fer",
        "DeadlyOre": "Minerai mortel",
        "Chest": "Coffre",
        "MidnightGrass": "Herbe de minuit",
        "MidnightSoil": "Terre fertile de minuit",
        "MidnightStone": "Roche de minuit",
        "MidnightWood": "Bois de minuit",
        "MidnightLeaf": "Feuille de minuit",
        "Bush": "Buisson",
        "MidnightBush": "Buisson de minuit",
        "RedFlower": "Fleur rouge",
        "WhiteFlower": "Fleur blanche",
        "BlueFlower": "Fleur bleue",
        "TallGrass": "Haute herbe",
        "Sand": "Sable",
        "Sandstone": "Grès",
        "Cactus": "Cactus",
        "Snow": "Neige",
        "Ice": "Glace",
        "SnowyBush": "Buisson enneigé",
        "Glass": "Verre",
        "SolenoidOre": "Minerai de solenoïde",
        "SnowyLeaf": "Feuille enneigé",
        "Pumpkin": "Citrouille",
        "JackOLantern": "Citrouille-lanterne",
        "PalmWood": "Bois de palmier",
        "PalmLeaf": "Feuille de palmier",
        "Water": "Eau",
        "Lava": "Lave",
        "GrassSlab": "Dalle d'herbe",
        "DirtSlab": "Dalle de terre",
        "StoneSlab": "Dalle de roche",
        "WoodSlab": "Dalle en bois",
        "MidnightStoneSlab": "Dalle de roche de minuit",
        "MidnightWoodSlab": "Dalle en bois de minuit",
        "SandstoneSlab": "Dalle de grès",
        "SnowSlab": "Dalle de neige",
        "IceSlab": "Dalle de glace",
        "GlassSlab": "Dalle de verre",
        "PalmWoodSlab": "Dalle en bois de palmier",
        "Stick": "Bâton",
        "Hammer": "Marteau",
        "IronPick": "Pioche en fer",
        "DeadlyPick": "Pioche mortelle",
        "IronAxe": "Hache en fer",
        "DeadlyAxe": "Hache mortelle",
        "Ultrahammer": "Ultra-marteau",
        "SolenoidCollector": "Collecteur électromagnétique",
        "Rock": "Pierre",
        "Hypersilk": "Hypersoie",
        "IronBars": "Lingot de fer",
        "DeadlyBars": "Lingot mortel",
        "SolenoidBars": "Lingot de solenoïde",
        "Compass": "Boussole",
        "4DGlasses": "Lunette 4D",
        "KleinBottle": "Bouteille de Klein",
        "HealthPotion": "Potion de soin",
        "RedLens": "Lentille rouge",
        "GreenLens": "Lentille verte",
        "BlueLens": "Lentille bleue",
        "Alidade": "Alidade",
        "Sign": "Pancarte",
        "Bucket": "Seau",
        "WaterBucket": "Seau d'eau",
        "LavaBucket": "Seau de lave"
    }
}