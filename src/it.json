{
    "LanguageName": "Italiano",

    "LoadingText": "Caricamento...",
    "OkButton": "OK",
    "CancelButton": "Cancella",
    "YesButton": "Sì",
    "NoButton": "No",
    "ContinueButton": "Continua",
    "BackButton": "Indietro",
    "ExitButton": "Esci",
    "EditButton": "Modifica",
    "SaveButton": "Salva",
    "CopyButton": "Copia",
    "ChooseFileButton": "Scegli File...",
    "DefaultValueText": "{} (predefinito)",
    "ErrorPopup": {
        "TitleText": "Errore!"
    },

    "TitleScreen": {
        "DemoText": "DEMO",
        "SingleplayerButton": "Giocatore Singolo",
        "MultiplayerButton": "Multigiocatore",
        "TutorialButton": "Tutorial",
        "SettingsButton": "Opzioni",
        "CreditsButton": "Riconoscimenti",
        "QuitButton": "Esci dal gioco"
    },

    "Singleplayer": {
        "YourWorldsText": "I tuoi Mondi:",
        "NoWorldsText": "Nessun mondo da caricare...",
        "InvalidWorldButton": "{} (non valido)",
        "OldWorldButton": "{} (vecchio)",
        "NewWorldButton": "Nuovo Mondo",
        "WorldConversionPopup": {
            "TitleText": "Convertire al Nuovo Formato del Mondo?",
            "OldWorldVersionText": "Il mondo \"{}\" non può essere caricato perché il formato del mondo è di una versione più vecchia ({}), la quale è incompatibile con l'attuale versione ({}).",
            "OldWorldVersionlessText": "Il mondo \"{}\" non può essere caricato perché il formato del mondo è di una versione più vecchia, la quale è incompatibile con l'attuale versione.",
            "ConversionPromptText": "Vuoi convertire il mondo alla versione attuale? Avrai prima la possibilità di eseguire una copia di backup."
        },
        "InvalidWorldPopup": {
            "TitleText": "Mondo non valido!",
            "InvalidVersionText": "Il mondo \"{}\" non può essere caricato perché è di una nuova versione di 4D Miner, oppure ha un numero di versione non valido."
        }
    },

    "Multiplayer": {
        "ServerAddressText": "Indirizzo Server:",
        "DisplayNameText": "Nome Utente:",
        "DisplayNameInput": "Giocatore",
        "UuidText": "UUID:",
        "UuidDescriptionText": "Il tuo UUID è un dato segreto che viene generato la prima volta che apri questo menù.\nGli UUID permettono di salvare i tuoi progressi nei server.\n\nNON condividere questo dato con alcun utente.\nPuoi trasferire un UUID da un'installazione precedente sostituendo/modificando il file \"uuid.txt\".",
        "JoinButton": "Entra!",
        "ChangeSkinButton": "Cambia Skin",
        "Status": {
            "ResolvingHostText": "Risoluzione dell'host...",
            "ConnectingText": "Connessione...",
            "AuthenticatingText": "Autenticazione...",
            "RemoteDisconnectedText": "Errore: La connessione è stata chiusa dal server.",
            "FailedToConnectText": "Errore: Impossibile connettersi.",
            "ErrorText": "Si è verificato un errore.",
            "LoadingTerrainText": "Caricamento del terreno...",
            "QueuePositionText": "Posizione nella coda: {}",
            "InvalidResponseText": "Errore: Risposta del server invalida",
            "IncompatibleVersionText": "Errore: La versione del gioco non corrisponde alla versione del server.\n\nVersioni compatibili: {}",
            "ServerFullText": "Errore: al momento il server è pieno.",
            "LostConnectionText": "Errore: Connessione persa"
        }
    },

    "CreateWorld": {
        "WorldNameText": "Nome del Mondo:",
        "WorldNameInput": "Nuovo Mondo",
        "WorldSeedText": "Seme del Mondo:",
        "FlatWorldCheckbox": "Mondo Piatto",
        "GenerateCavesCheckbox": "Genera le Caverne (più lento)",
        "CaveSizeSlider": "Dimensione delle Caverne: {}%",
        "CaveFrequencySlider": "Frequenza delle Caverne: {}%",
        "CaveDetailLevelSlider": "Livello di Dettaglio delle Caverne: {} (influenza seriamente le prestazioni)",
        "BiomeSizeSlider": "Dimensione dei Biomi: {}%",
        "TerrainAmplificationSlider": "Amplificazione del Terreno: {}%",
        "CreateNewWorldButton": "Crea Nuovo Mondo!",
        "NoNamePopup": {
            "NoNameText": "Non puoi creare un mondo senza un nome!"
        }
    },

    "WorldConverter": {
        "CloseWarningText": "NON CHIUDERE il gioco!",
        "BackupWorldTitle": "{} (copia di backup)",
        "BackupStatusText": "Creazione di copia di backup del mondo \"{}\"...",
        "ConversionStatusText": "Conversione del mondo \"{}\" dalla versione {} alla versione {}...",
        "ConversionFinishedText": "Fatto!",
        "BackupPopup": {
            "TitleText": "Creare una copia di backup dei file di \"{}\"?",
            "PromptText": "I dati di salvataggio del mondo \"{}\" potrebbero essere danneggiati dal processo di conversione del mondo. Vuoi prima creare una copia di backup?"
        },
        "ErrorPopup": {
            "GenericConversionErrorText": "Si è verificato un errore durante l'aggiornamento del mondo alla versione {}.",
            "InvalidObjectText": "Impossibile copiare l'oggetto non valido \"{}\" (è prevista una cartella o un file corretto).",
            "CopyErrorText": "Impossibile copiare l'oggetto \"{}\": \"{}\"",
            "MissingChunksDirText": "Cartella \"chunks\" non trovata.",
            "UnableToOpenFileText": "Impossibile aprire il file \"{}\"."
        },
        "ConversionStatus": {
            "CleaningUpText": "Pulizia in corso..."
        }
    },

    "WorldGen": {
        "ProgressText": "{}/{} chunks caricati"
    },

    "Settings": {
        "ControlsButton": "Impostazioni Mouse e Tastiera",
        "FullscreenButton": {
            "EnterText": "Abilita schermo intero",
            "ExitText": "Disabilita da schermo intero"
        },
        "RenderDistanceSlider": "Distanza di Resa: {}",
        "ScrollSensitivitySlider": "Sensibilità Scorrimento:",
        "LookSensitivitySlider": "Sensibilità:",
        "InvertLookXCheckbox": "Inverti Asse X",
        "InvertLookYCheckbox": "Inverti Asse Y",
        "FovSlider": {
            "LabelText": "FOV: {}",
            "ValueText": "{} gradi"
        },
        "DifficultySlider": {
            "LabelText": "Difficoltà: {}",
            "EasyText": "Facile (niente mostri)",
            "NormalText": "Normale",
            "HardText": "Difficile"
        },
        "SmoothLightingCheckbox": "Luce Soffusa",
        "ShadowsCheckbox": "Ombre",
        "ColoredLightsCheckbox": "Luci Colorate",
        "Forg": {
            "LabelText": "Raan: {}",
            "Names": {
                "Frank": "Frank",
                "Fernanda": "Fernanda"
            }
        },
        "Audio": {
            "LabelText": "Audio:",
            "GlobalVolumeSlider": "Volume Globale:",
            "MusicVolumeSlider": "Volume Musica:",
            "CreatureVolumeSlider": "Volume Creature:",
            "AmbienceVolumeSlider": "Volume Ambiente:"
        },
        "Multiplayer": {
            "LabelText": "Multigiocatore:",
            "ChatCheckbox": "Mostra Chat",
            "NametagsCheckbox": "Mostra Nomi dei Giocatori",
            "SkinsCheckbox": "Mostra Skin Personalizzate"
        }
    },

    "Tutorial": {
        "Slideshow": {
            "Slide1": {
                "Text1": "Questo tutorial ti aiuterà a capire lo spazio quadridimensionale.\n\n\n\nFacendo un paragone con le differenze tra spazio 2D e 3D, è possibile intuire come lo spazio 3D è correlato a quello 4D."
            },
            "Slide2": {
                "Text1": "Ti presento {} la raan (sì, una RAAN)",
                "Text2": "Le raen sono esseri bidimensionali, il che significa che possono solo esistere in un piano di 2 dimensioni, ciò però non vuol dire che loro non possano esplorare la terza dimensione!\n\n\n\nLe raen possono muoversi all'interno di qualsiasi sezione trasversale 2D del mondo 3D circostante."
            },
            "Slide3": {
                "Text1": "Le raen hanno anche l'abilità speciale di ruotare i loro piani di esistenza nella terza dimensione, permettendo loro di vedere qualsiasi sezione trasversale vogliano!",
                "Text2": "Nella parte successiva, sarai in grado di controllare {} e provare questa abilità direttamente!"
            },
            "Slide4": {
                "Text1": "La stessa logica si applica per la terza e la quarta dimensioni.\n\n\nPuoi scorrere con il mouse per ruotare la tua visuale del mondo quadridimensionale ed vedere aspetti del terreno totalmente nuovi!",
                "Text2": "Ora sai come giocare a 4D Miner! Se vuoi aiuto per orientarti attraverso la quarta dimensione, vai alla ricerca della",
                "Text3": "bussola!"
            }
        },
        "ForgGame": {
            "2DViewText": "WASD o tasti frecce per muovere {} la raan.\n\nScorri con il mouse per ruotare la sezione trasversale 2D!",
            "3DViewText": "Clicca e trascina per regolare la visuale 3D."
        }
    },

    "Game": {
        "Chat": {
            "LabelText": "Chat:"
        },
        "CraftingMenu": {
            "LabelText": "Fabbricazione:"
        },
        "Compass": {
            "PositionText": "Posizione: {}, {}, {}, {}",
            "FacingDirectionText": "Direzione Frontale: {}, {}, {}, {}",
            "XText": "X: {}",
            "YText": "Y: {}",
            "ZText": "Z: {}",
            "WText": "W: {}"
        },
        "Chest": {
            "LabelText": "Baule:"
        },
        "Sign": {
            "LabelText": "Cartello:"
        }
    },

    "Pause": {
        "PausedText": "In pausa...",
        "BackToGameButton": "Torna al Gioco...",
        "QuitToTitleButton": "Salva ed Esci"
    },

    "DeathScreen": {
        "RetryButton": "Riprova..."
    },

    "Items": {
        "Air": "Aria",
        "Grass": "Erba",
        "Dirt": "Terra",
        "Stone": "Pietra",
        "Wood": "Legno",
        "Leaf": "Foglia",
        "SemiMoltenRock": "Roccia Semi-fusa",
        "IronOre": "Minerale di Ferro grezzo",
        "DeadlyOre": "Minerale di Letalio grezzo",
        "Chest": "Baule",
        "MidnightGrass": "Erba della Mezzanotte",
        "MidnightSoil": "Terra della Mezzanotte",
        "MidnightStone": "Pietra della Mezzanotte",
        "MidnightWood": "Legno di Mezzanotte",
        "MidnightLeaf": "Foglia di Mezzanotte",
        "Bush": "Cespuglio",
        "MidnightBush": "Cespuglio della Mezzanotte",
        "RedFlower": "Fiore Rosso",
        "WhiteFlower": "Fiore Bianco",
        "BlueFlower": "Fiore Blu",
        "TallGrass": "Erba Alta",
        "Sand": "Sabbia",
        "Sandstone": "Arenaria",
        "Cactus": "Cactus",
        "Snow": "Neve",
        "Ice": "Ghiaccio",
        "SnowyBush": "Cesbuglio Innevato",
        "Glass": "Vetro",
        "SolenoidOre": "Minerale di Solenoide grezzo",
        "SnowyLeaf": "Foglia Innevata",
        "Pumpkin": "Zucca",
        "JackOLantern": "Zucca di Halloween",
        "PalmWood": "Legno di Palma",
        "PalmLeaf": "Foglia di Palma",
        "Water": "Acqua",
        "Lava": "Lava",
        "VolcanicRock": "Roccia Vulcanica",
        "GrassSlab": "Lastra d'Erba",
        "DirtSlab": "Lastra di Terra",
        "StoneSlab": "Lastra di Pietra",
        "WoodSlab": "Lastra di Legno",
        "SemiMoltenRockSlab": "Lastra di Roccia Semi-fusa",
        "MidnightStoneSlab": "Lastra di Pietra della Mezzanotte",
        "MidnightWoodSlab": "Lastra di Legno di Mezzanotte",
        "SandstoneSlab": "Lastra di Arenaria",
        "SnowSlab": "Lastra di Neve",
        "IceSlab": "Lastra di Ghiaccio",
        "GlassSlab": "Lastra di Vetro",
        "PalmWoodSlab": "Lastra di Legno di Palma",
        "VolcanicRockSlab": "Lastra di Roccia Vulcanica",
        "Stick": "Bastoncino",
        "Hammer": "Martello",
        "IronPick": "Piccone di Ferro",
        "DeadlyPick": "Piccone di Letalio",
        "IronAxe": "Ascia di Ferro",
        "DeadlyAxe": "Ascia di Letalio",
        "Ultrahammer": "Ultramartello",
        "SolenoidCollector": "Collettore a Solenoide",
        "Rock": "Roccia",
        "Hypersilk": "Iperseta",
        "IronBars": "Barre di Ferro",
        "DeadlyBars": "Barre di Letalio",
        "SolenoidBars": "Barre di Solenoide",
        "Compass": "Bussola",
        "4DGlasses": "Occhiali 4D",
        "KleinBottle": "Bottiglia di Klein",
        "HealthPotion": "Pozione di Salute",
        "RedLens": "Lente Rossa",
        "GreenLens": "Lente Verde",
        "BlueLens": "Lente Blu",
        "Alidade": "Alidada",
        "Sign": "Cartello",
        "Bucket": "Secchio",
        "WaterBucket": "Secchio d'Acqua",
        "LavaBucket": "Secchio di Lava"
    }
}